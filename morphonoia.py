#

BLANK_STR = '______'


class ThoughtForm:

    def __init__(
            self,
            name: str,
            arguments: tp.Iterable(ThoughtForm),
            *,
            description: str='',
            **kwargs
        ):
        self.name = name
        self.arguments = arguments
        self.arity = len(arguments)
        self.description = description


    @classmethod
    def read_class(cls):
        _NAME = ' ' + cls.NAME + ' '
        return _NAME.join(cls.BLANK_STR for _ in range(cls.ARITY))

    def read_instance(self):
        print('trying it out')






class ThoughtForm(Read):
    ARITY = None
    NAME = ''
    DESCRIPTION = ''


    def __init__(self, *args):
        pass

    @classmethod
    def read_class(cls):
        _NAME = ' ' + cls.NAME + ' '
        return _NAME.join(cls.BLANK_STR for _ in range(cls.ARITY))

    def read_instance(self):
        print('trying it out')

    # def form(self):
    #     tree = getattr(self, '')

    # @classmethod
    # def def(cls):
    #     pass

    # @classmethod
    # def traverse_inorder():
    #     '''traverse the planar tree formed'''
    #     pass


# class CustomReadMethod:
#     def __get__(self, inst, cls):
#         if inst:
#             return lambda: inst.read_instance()
#         return lambda: cls.read_class()

# class Read:
#     read = CustomReadMethod()
